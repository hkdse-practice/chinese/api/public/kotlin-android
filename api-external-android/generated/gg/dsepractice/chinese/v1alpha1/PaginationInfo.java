// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: dsepractice/chinese/v1alpha1/core_types.proto

package gg.dsepractice.chinese.v1alpha1;

/**
 * Protobuf type {@code dsepractice.chinese.v1alpha1.PaginationInfo}
 */
public  final class PaginationInfo extends
    com.google.protobuf.GeneratedMessageLite<
        PaginationInfo, PaginationInfo.Builder> implements
    // @@protoc_insertion_point(message_implements:dsepractice.chinese.v1alpha1.PaginationInfo)
    PaginationInfoOrBuilder {
  private PaginationInfo() {
    cursor_ = "";
  }
  private int bitField0_;
  public static final int HAS_NEXT_PAGE_FIELD_NUMBER = 1;
  private boolean hasNextPage_;
  /**
   * <code>bool has_next_page = 1 [json_name = "hasNextPage"];</code>
   * @return The hasNextPage.
   */
  @java.lang.Override
  public boolean getHasNextPage() {
    return hasNextPage_;
  }
  /**
   * <code>bool has_next_page = 1 [json_name = "hasNextPage"];</code>
   * @param value The hasNextPage to set.
   */
  private void setHasNextPage(boolean value) {
    
    hasNextPage_ = value;
  }
  /**
   * <code>bool has_next_page = 1 [json_name = "hasNextPage"];</code>
   */
  private void clearHasNextPage() {
    
    hasNextPage_ = false;
  }

  public static final int CURSOR_FIELD_NUMBER = 2;
  private java.lang.String cursor_;
  /**
   * <code>optional string cursor = 2 [json_name = "cursor"];</code>
   * @return Whether the cursor field is set.
   */
  @java.lang.Override
  public boolean hasCursor() {
    return ((bitField0_ & 0x00000001) != 0);
  }
  /**
   * <code>optional string cursor = 2 [json_name = "cursor"];</code>
   * @return The cursor.
   */
  @java.lang.Override
  public java.lang.String getCursor() {
    return cursor_;
  }
  /**
   * <code>optional string cursor = 2 [json_name = "cursor"];</code>
   * @return The bytes for cursor.
   */
  @java.lang.Override
  public com.google.protobuf.ByteString
      getCursorBytes() {
    return com.google.protobuf.ByteString.copyFromUtf8(cursor_);
  }
  /**
   * <code>optional string cursor = 2 [json_name = "cursor"];</code>
   * @param value The cursor to set.
   */
  private void setCursor(
      java.lang.String value) {
    java.lang.Class<?> valueClass = value.getClass();
  bitField0_ |= 0x00000001;
    cursor_ = value;
  }
  /**
   * <code>optional string cursor = 2 [json_name = "cursor"];</code>
   */
  private void clearCursor() {
    bitField0_ = (bitField0_ & ~0x00000001);
    cursor_ = getDefaultInstance().getCursor();
  }
  /**
   * <code>optional string cursor = 2 [json_name = "cursor"];</code>
   * @param value The bytes for cursor to set.
   */
  private void setCursorBytes(
      com.google.protobuf.ByteString value) {
    checkByteStringIsUtf8(value);
    cursor_ = value.toStringUtf8();
    bitField0_ |= 0x00000001;
  }

  public static gg.dsepractice.chinese.v1alpha1.PaginationInfo parseFrom(
      java.nio.ByteBuffer data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return com.google.protobuf.GeneratedMessageLite.parseFrom(
        DEFAULT_INSTANCE, data);
  }
  public static gg.dsepractice.chinese.v1alpha1.PaginationInfo parseFrom(
      java.nio.ByteBuffer data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return com.google.protobuf.GeneratedMessageLite.parseFrom(
        DEFAULT_INSTANCE, data, extensionRegistry);
  }
  public static gg.dsepractice.chinese.v1alpha1.PaginationInfo parseFrom(
      com.google.protobuf.ByteString data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return com.google.protobuf.GeneratedMessageLite.parseFrom(
        DEFAULT_INSTANCE, data);
  }
  public static gg.dsepractice.chinese.v1alpha1.PaginationInfo parseFrom(
      com.google.protobuf.ByteString data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return com.google.protobuf.GeneratedMessageLite.parseFrom(
        DEFAULT_INSTANCE, data, extensionRegistry);
  }
  public static gg.dsepractice.chinese.v1alpha1.PaginationInfo parseFrom(byte[] data)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return com.google.protobuf.GeneratedMessageLite.parseFrom(
        DEFAULT_INSTANCE, data);
  }
  public static gg.dsepractice.chinese.v1alpha1.PaginationInfo parseFrom(
      byte[] data,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws com.google.protobuf.InvalidProtocolBufferException {
    return com.google.protobuf.GeneratedMessageLite.parseFrom(
        DEFAULT_INSTANCE, data, extensionRegistry);
  }
  public static gg.dsepractice.chinese.v1alpha1.PaginationInfo parseFrom(java.io.InputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageLite.parseFrom(
        DEFAULT_INSTANCE, input);
  }
  public static gg.dsepractice.chinese.v1alpha1.PaginationInfo parseFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageLite.parseFrom(
        DEFAULT_INSTANCE, input, extensionRegistry);
  }
  public static gg.dsepractice.chinese.v1alpha1.PaginationInfo parseDelimitedFrom(java.io.InputStream input)
      throws java.io.IOException {
    return parseDelimitedFrom(DEFAULT_INSTANCE, input);
  }
  public static gg.dsepractice.chinese.v1alpha1.PaginationInfo parseDelimitedFrom(
      java.io.InputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return parseDelimitedFrom(DEFAULT_INSTANCE, input, extensionRegistry);
  }
  public static gg.dsepractice.chinese.v1alpha1.PaginationInfo parseFrom(
      com.google.protobuf.CodedInputStream input)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageLite.parseFrom(
        DEFAULT_INSTANCE, input);
  }
  public static gg.dsepractice.chinese.v1alpha1.PaginationInfo parseFrom(
      com.google.protobuf.CodedInputStream input,
      com.google.protobuf.ExtensionRegistryLite extensionRegistry)
      throws java.io.IOException {
    return com.google.protobuf.GeneratedMessageLite.parseFrom(
        DEFAULT_INSTANCE, input, extensionRegistry);
  }

  public static Builder newBuilder() {
    return (Builder) DEFAULT_INSTANCE.createBuilder();
  }
  public static Builder newBuilder(gg.dsepractice.chinese.v1alpha1.PaginationInfo prototype) {
    return (Builder) DEFAULT_INSTANCE.createBuilder(prototype);
  }

  /**
   * Protobuf type {@code dsepractice.chinese.v1alpha1.PaginationInfo}
   */
  public static final class Builder extends
      com.google.protobuf.GeneratedMessageLite.Builder<
        gg.dsepractice.chinese.v1alpha1.PaginationInfo, Builder> implements
      // @@protoc_insertion_point(builder_implements:dsepractice.chinese.v1alpha1.PaginationInfo)
      gg.dsepractice.chinese.v1alpha1.PaginationInfoOrBuilder {
    // Construct using gg.dsepractice.chinese.v1alpha1.PaginationInfo.newBuilder()
    private Builder() {
      super(DEFAULT_INSTANCE);
    }


    /**
     * <code>bool has_next_page = 1 [json_name = "hasNextPage"];</code>
     * @return The hasNextPage.
     */
    @java.lang.Override
    public boolean getHasNextPage() {
      return instance.getHasNextPage();
    }
    /**
     * <code>bool has_next_page = 1 [json_name = "hasNextPage"];</code>
     * @param value The hasNextPage to set.
     * @return This builder for chaining.
     */
    public Builder setHasNextPage(boolean value) {
      copyOnWrite();
      instance.setHasNextPage(value);
      return this;
    }
    /**
     * <code>bool has_next_page = 1 [json_name = "hasNextPage"];</code>
     * @return This builder for chaining.
     */
    public Builder clearHasNextPage() {
      copyOnWrite();
      instance.clearHasNextPage();
      return this;
    }

    /**
     * <code>optional string cursor = 2 [json_name = "cursor"];</code>
     * @return Whether the cursor field is set.
     */
    @java.lang.Override
    public boolean hasCursor() {
      return instance.hasCursor();
    }
    /**
     * <code>optional string cursor = 2 [json_name = "cursor"];</code>
     * @return The cursor.
     */
    @java.lang.Override
    public java.lang.String getCursor() {
      return instance.getCursor();
    }
    /**
     * <code>optional string cursor = 2 [json_name = "cursor"];</code>
     * @return The bytes for cursor.
     */
    @java.lang.Override
    public com.google.protobuf.ByteString
        getCursorBytes() {
      return instance.getCursorBytes();
    }
    /**
     * <code>optional string cursor = 2 [json_name = "cursor"];</code>
     * @param value The cursor to set.
     * @return This builder for chaining.
     */
    public Builder setCursor(
        java.lang.String value) {
      copyOnWrite();
      instance.setCursor(value);
      return this;
    }
    /**
     * <code>optional string cursor = 2 [json_name = "cursor"];</code>
     * @return This builder for chaining.
     */
    public Builder clearCursor() {
      copyOnWrite();
      instance.clearCursor();
      return this;
    }
    /**
     * <code>optional string cursor = 2 [json_name = "cursor"];</code>
     * @param value The bytes for cursor to set.
     * @return This builder for chaining.
     */
    public Builder setCursorBytes(
        com.google.protobuf.ByteString value) {
      copyOnWrite();
      instance.setCursorBytes(value);
      return this;
    }

    // @@protoc_insertion_point(builder_scope:dsepractice.chinese.v1alpha1.PaginationInfo)
  }
  @java.lang.Override
  @java.lang.SuppressWarnings({"unchecked", "fallthrough"})
  protected final java.lang.Object dynamicMethod(
      com.google.protobuf.GeneratedMessageLite.MethodToInvoke method,
      java.lang.Object arg0, java.lang.Object arg1) {
    switch (method) {
      case NEW_MUTABLE_INSTANCE: {
        return new gg.dsepractice.chinese.v1alpha1.PaginationInfo();
      }
      case NEW_BUILDER: {
        return new Builder();
      }
      case BUILD_MESSAGE_INFO: {
          java.lang.Object[] objects = new java.lang.Object[] {
            "bitField0_",
            "hasNextPage_",
            "cursor_",
          };
          java.lang.String info =
              "\u0000\u0002\u0000\u0001\u0001\u0002\u0002\u0000\u0000\u0000\u0001\u0007\u0002\u1208" +
              "\u0000";
          return newMessageInfo(DEFAULT_INSTANCE, info, objects);
      }
      // fall through
      case GET_DEFAULT_INSTANCE: {
        return DEFAULT_INSTANCE;
      }
      case GET_PARSER: {
        com.google.protobuf.Parser<gg.dsepractice.chinese.v1alpha1.PaginationInfo> parser = PARSER;
        if (parser == null) {
          synchronized (gg.dsepractice.chinese.v1alpha1.PaginationInfo.class) {
            parser = PARSER;
            if (parser == null) {
              parser =
                  new DefaultInstanceBasedParser<gg.dsepractice.chinese.v1alpha1.PaginationInfo>(
                      DEFAULT_INSTANCE);
              PARSER = parser;
            }
          }
        }
        return parser;
    }
    case GET_MEMOIZED_IS_INITIALIZED: {
      return (byte) 1;
    }
    case SET_MEMOIZED_IS_INITIALIZED: {
      return null;
    }
    }
    throw new UnsupportedOperationException();
  }


  // @@protoc_insertion_point(class_scope:dsepractice.chinese.v1alpha1.PaginationInfo)
  private static final gg.dsepractice.chinese.v1alpha1.PaginationInfo DEFAULT_INSTANCE;
  static {
    PaginationInfo defaultInstance = new PaginationInfo();
    // New instances are implicitly immutable so no need to make
    // immutable.
    DEFAULT_INSTANCE = defaultInstance;
    com.google.protobuf.GeneratedMessageLite.registerDefaultInstance(
      PaginationInfo.class, defaultInstance);
  }

  public static gg.dsepractice.chinese.v1alpha1.PaginationInfo getDefaultInstance() {
    return DEFAULT_INSTANCE;
  }

  private static volatile com.google.protobuf.Parser<PaginationInfo> PARSER;

  public static com.google.protobuf.Parser<PaginationInfo> parser() {
    return DEFAULT_INSTANCE.getParserForType();
  }
}


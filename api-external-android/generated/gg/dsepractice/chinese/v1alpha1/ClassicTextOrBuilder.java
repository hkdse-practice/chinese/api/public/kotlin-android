// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: dsepractice/chinese/v1alpha1/text_types.proto

package gg.dsepractice.chinese.v1alpha1;

public interface ClassicTextOrBuilder extends
    // @@protoc_insertion_point(interface_extends:dsepractice.chinese.v1alpha1.ClassicText)
    com.google.protobuf.MessageLiteOrBuilder {

  /**
   * <code>string name = 1 [json_name = "name"];</code>
   * @return The name.
   */
  java.lang.String getName();
  /**
   * <code>string name = 1 [json_name = "name"];</code>
   * @return The bytes for name.
   */
  com.google.protobuf.ByteString
      getNameBytes();

  /**
   * <code>repeated string all_text_segments = 2 [json_name = "allTextSegments"];</code>
   * @return A list containing the allTextSegments.
   */
  java.util.List<java.lang.String>
      getAllTextSegmentsList();
  /**
   * <code>repeated string all_text_segments = 2 [json_name = "allTextSegments"];</code>
   * @return The count of allTextSegments.
   */
  int getAllTextSegmentsCount();
  /**
   * <code>repeated string all_text_segments = 2 [json_name = "allTextSegments"];</code>
   * @param index The index of the element to return.
   * @return The allTextSegments at the given index.
   */
  java.lang.String getAllTextSegments(int index);
  /**
   * <code>repeated string all_text_segments = 2 [json_name = "allTextSegments"];</code>
   * @param index The index of the element to return.
   * @return The allTextSegments at the given index.
   */
  com.google.protobuf.ByteString
      getAllTextSegmentsBytes(int index);

  /**
   * <code>repeated string all_words = 3 [json_name = "allWords"];</code>
   * @return A list containing the allWords.
   */
  java.util.List<java.lang.String>
      getAllWordsList();
  /**
   * <code>repeated string all_words = 3 [json_name = "allWords"];</code>
   * @return The count of allWords.
   */
  int getAllWordsCount();
  /**
   * <code>repeated string all_words = 3 [json_name = "allWords"];</code>
   * @param index The index of the element to return.
   * @return The allWords at the given index.
   */
  java.lang.String getAllWords(int index);
  /**
   * <code>repeated string all_words = 3 [json_name = "allWords"];</code>
   * @param index The index of the element to return.
   * @return The allWords at the given index.
   */
  com.google.protobuf.ByteString
      getAllWordsBytes(int index);

  /**
   * <code>repeated .dsepractice.chinese.v1alpha1.ParagraphNode nodes = 4 [json_name = "nodes"];</code>
   */
  java.util.List<gg.dsepractice.chinese.v1alpha1.ParagraphNode> 
      getNodesList();
  /**
   * <code>repeated .dsepractice.chinese.v1alpha1.ParagraphNode nodes = 4 [json_name = "nodes"];</code>
   */
  gg.dsepractice.chinese.v1alpha1.ParagraphNode getNodes(int index);
  /**
   * <code>repeated .dsepractice.chinese.v1alpha1.ParagraphNode nodes = 4 [json_name = "nodes"];</code>
   */
  int getNodesCount();
}

//Generated by the protocol buffer compiler. DO NOT EDIT!
// source: dsepractice/chinese/v1alpha1/core_types.proto

package gg.dsepractice.chinese.v1alpha1;

@kotlin.jvm.JvmName("-initializelistGameSessionsResponse")
inline fun listGameSessionsResponse(block: gg.dsepractice.chinese.v1alpha1.ListGameSessionsResponseKt.Dsl.() -> kotlin.Unit): gg.dsepractice.chinese.v1alpha1.ListGameSessionsResponse =
  gg.dsepractice.chinese.v1alpha1.ListGameSessionsResponseKt.Dsl._create(gg.dsepractice.chinese.v1alpha1.ListGameSessionsResponse.newBuilder()).apply { block() }._build()
object ListGameSessionsResponseKt {
  @kotlin.OptIn(com.google.protobuf.kotlin.OnlyForUseByGeneratedProtoCode::class)
  @com.google.protobuf.kotlin.ProtoDslMarker
  class Dsl private constructor(
    private val _builder: gg.dsepractice.chinese.v1alpha1.ListGameSessionsResponse.Builder
  ) {
    companion object {
      @kotlin.jvm.JvmSynthetic
      @kotlin.PublishedApi
      internal fun _create(builder: gg.dsepractice.chinese.v1alpha1.ListGameSessionsResponse.Builder): Dsl = Dsl(builder)
    }

    @kotlin.jvm.JvmSynthetic
    @kotlin.PublishedApi
    internal fun _build(): gg.dsepractice.chinese.v1alpha1.ListGameSessionsResponse = _builder.build()

    /**
     * An uninstantiable, behaviorless type to represent the field in
     * generics.
     */
    @kotlin.OptIn(com.google.protobuf.kotlin.OnlyForUseByGeneratedProtoCode::class)
    class GameSessionsProxy private constructor() : com.google.protobuf.kotlin.DslProxy()
    /**
     * <code>repeated .dsepractice.chinese.v1alpha1.GameSession game_sessions = 1 [json_name = "gameSessions"];</code>
     */
     val gameSessions: com.google.protobuf.kotlin.DslList<gg.dsepractice.chinese.v1alpha1.GameSession, GameSessionsProxy>
      @kotlin.jvm.JvmSynthetic
      get() = com.google.protobuf.kotlin.DslList(
        _builder.getGameSessionsList()
      )
    /**
     * <code>repeated .dsepractice.chinese.v1alpha1.GameSession game_sessions = 1 [json_name = "gameSessions"];</code>
     * @param value The gameSessions to add.
     */
    @kotlin.jvm.JvmSynthetic
    @kotlin.jvm.JvmName("addGameSessions")
    fun com.google.protobuf.kotlin.DslList<gg.dsepractice.chinese.v1alpha1.GameSession, GameSessionsProxy>.add(value: gg.dsepractice.chinese.v1alpha1.GameSession) {
      _builder.addGameSessions(value)
    }
    /**
     * <code>repeated .dsepractice.chinese.v1alpha1.GameSession game_sessions = 1 [json_name = "gameSessions"];</code>
     * @param value The gameSessions to add.
     */
    @kotlin.jvm.JvmSynthetic
    @kotlin.jvm.JvmName("plusAssignGameSessions")
    @Suppress("NOTHING_TO_INLINE")
    inline operator fun com.google.protobuf.kotlin.DslList<gg.dsepractice.chinese.v1alpha1.GameSession, GameSessionsProxy>.plusAssign(value: gg.dsepractice.chinese.v1alpha1.GameSession) {
      add(value)
    }
    /**
     * <code>repeated .dsepractice.chinese.v1alpha1.GameSession game_sessions = 1 [json_name = "gameSessions"];</code>
     * @param values The gameSessions to add.
     */
    @kotlin.jvm.JvmSynthetic
    @kotlin.jvm.JvmName("addAllGameSessions")
    fun com.google.protobuf.kotlin.DslList<gg.dsepractice.chinese.v1alpha1.GameSession, GameSessionsProxy>.addAll(values: kotlin.collections.Iterable<gg.dsepractice.chinese.v1alpha1.GameSession>) {
      _builder.addAllGameSessions(values)
    }
    /**
     * <code>repeated .dsepractice.chinese.v1alpha1.GameSession game_sessions = 1 [json_name = "gameSessions"];</code>
     * @param values The gameSessions to add.
     */
    @kotlin.jvm.JvmSynthetic
    @kotlin.jvm.JvmName("plusAssignAllGameSessions")
    @Suppress("NOTHING_TO_INLINE")
    inline operator fun com.google.protobuf.kotlin.DslList<gg.dsepractice.chinese.v1alpha1.GameSession, GameSessionsProxy>.plusAssign(values: kotlin.collections.Iterable<gg.dsepractice.chinese.v1alpha1.GameSession>) {
      addAll(values)
    }
    /**
     * <code>repeated .dsepractice.chinese.v1alpha1.GameSession game_sessions = 1 [json_name = "gameSessions"];</code>
     * @param index The index to set the value at.
     * @param value The gameSessions to set.
     */
    @kotlin.jvm.JvmSynthetic
    @kotlin.jvm.JvmName("setGameSessions")
    operator fun com.google.protobuf.kotlin.DslList<gg.dsepractice.chinese.v1alpha1.GameSession, GameSessionsProxy>.set(index: kotlin.Int, value: gg.dsepractice.chinese.v1alpha1.GameSession) {
      _builder.setGameSessions(index, value)
    }
    /**
     * <code>repeated .dsepractice.chinese.v1alpha1.GameSession game_sessions = 1 [json_name = "gameSessions"];</code>
     */
    @kotlin.jvm.JvmSynthetic
    @kotlin.jvm.JvmName("clearGameSessions")
    fun com.google.protobuf.kotlin.DslList<gg.dsepractice.chinese.v1alpha1.GameSession, GameSessionsProxy>.clear() {
      _builder.clearGameSessions()
    }

    /**
     * <code>.dsepractice.chinese.v1alpha1.PaginationInfo pagination_info = 2 [json_name = "paginationInfo"];</code>
     */
    var paginationInfo: gg.dsepractice.chinese.v1alpha1.PaginationInfo
      @JvmName("getPaginationInfo")
      get() = _builder.getPaginationInfo()
      @JvmName("setPaginationInfo")
      set(value) {
        _builder.setPaginationInfo(value)
      }
    /**
     * <code>.dsepractice.chinese.v1alpha1.PaginationInfo pagination_info = 2 [json_name = "paginationInfo"];</code>
     */
    fun clearPaginationInfo() {
      _builder.clearPaginationInfo()
    }
    /**
     * <code>.dsepractice.chinese.v1alpha1.PaginationInfo pagination_info = 2 [json_name = "paginationInfo"];</code>
     * @return Whether the paginationInfo field is set.
     */
    fun hasPaginationInfo(): kotlin.Boolean {
      return _builder.hasPaginationInfo()
    }
  }
}
inline fun gg.dsepractice.chinese.v1alpha1.ListGameSessionsResponse.copy(block: gg.dsepractice.chinese.v1alpha1.ListGameSessionsResponseKt.Dsl.() -> kotlin.Unit): gg.dsepractice.chinese.v1alpha1.ListGameSessionsResponse =
  gg.dsepractice.chinese.v1alpha1.ListGameSessionsResponseKt.Dsl._create(this.toBuilder()).apply { block() }._build()

val gg.dsepractice.chinese.v1alpha1.ListGameSessionsResponseOrBuilder.paginationInfoOrNull: gg.dsepractice.chinese.v1alpha1.PaginationInfo?
  get() = if (hasPaginationInfo()) getPaginationInfo() else null


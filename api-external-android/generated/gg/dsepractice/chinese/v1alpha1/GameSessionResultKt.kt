//Generated by the protocol buffer compiler. DO NOT EDIT!
// source: dsepractice/chinese/v1alpha1/core_types.proto

package gg.dsepractice.chinese.v1alpha1;

@kotlin.jvm.JvmName("-initializegameSessionResult")
inline fun gameSessionResult(block: gg.dsepractice.chinese.v1alpha1.GameSessionResultKt.Dsl.() -> kotlin.Unit): gg.dsepractice.chinese.v1alpha1.GameSessionResult =
  gg.dsepractice.chinese.v1alpha1.GameSessionResultKt.Dsl._create(gg.dsepractice.chinese.v1alpha1.GameSessionResult.newBuilder()).apply { block() }._build()
object GameSessionResultKt {
  @kotlin.OptIn(com.google.protobuf.kotlin.OnlyForUseByGeneratedProtoCode::class)
  @com.google.protobuf.kotlin.ProtoDslMarker
  class Dsl private constructor(
    private val _builder: gg.dsepractice.chinese.v1alpha1.GameSessionResult.Builder
  ) {
    companion object {
      @kotlin.jvm.JvmSynthetic
      @kotlin.PublishedApi
      internal fun _create(builder: gg.dsepractice.chinese.v1alpha1.GameSessionResult.Builder): Dsl = Dsl(builder)
    }

    @kotlin.jvm.JvmSynthetic
    @kotlin.PublishedApi
    internal fun _build(): gg.dsepractice.chinese.v1alpha1.GameSessionResult = _builder.build()

    /**
     * <code>.google.protobuf.Duration time_taken = 1 [json_name = "timeTaken"];</code>
     */
    var timeTaken: com.google.protobuf.Duration
      @JvmName("getTimeTaken")
      get() = _builder.getTimeTaken()
      @JvmName("setTimeTaken")
      set(value) {
        _builder.setTimeTaken(value)
      }
    /**
     * <code>.google.protobuf.Duration time_taken = 1 [json_name = "timeTaken"];</code>
     */
    fun clearTimeTaken() {
      _builder.clearTimeTaken()
    }
    /**
     * <code>.google.protobuf.Duration time_taken = 1 [json_name = "timeTaken"];</code>
     * @return Whether the timeTaken field is set.
     */
    fun hasTimeTaken(): kotlin.Boolean {
      return _builder.hasTimeTaken()
    }

    /**
     * <code>bool success = 2 [json_name = "success"];</code>
     */
    var success: kotlin.Boolean
      @JvmName("getSuccess")
      get() = _builder.getSuccess()
      @JvmName("setSuccess")
      set(value) {
        _builder.setSuccess(value)
      }
    /**
     * <code>bool success = 2 [json_name = "success"];</code>
     */
    fun clearSuccess() {
      _builder.clearSuccess()
    }

    /**
     * <code>int64 score = 3 [json_name = "score"];</code>
     */
    var score: kotlin.Long
      @JvmName("getScore")
      get() = _builder.getScore()
      @JvmName("setScore")
      set(value) {
        _builder.setScore(value)
      }
    /**
     * <code>int64 score = 3 [json_name = "score"];</code>
     */
    fun clearScore() {
      _builder.clearScore()
    }

    /**
     * <code>.google.protobuf.Timestamp submitted_at = 4 [json_name = "submittedAt"];</code>
     */
    var submittedAt: com.google.protobuf.Timestamp
      @JvmName("getSubmittedAt")
      get() = _builder.getSubmittedAt()
      @JvmName("setSubmittedAt")
      set(value) {
        _builder.setSubmittedAt(value)
      }
    /**
     * <code>.google.protobuf.Timestamp submitted_at = 4 [json_name = "submittedAt"];</code>
     */
    fun clearSubmittedAt() {
      _builder.clearSubmittedAt()
    }
    /**
     * <code>.google.protobuf.Timestamp submitted_at = 4 [json_name = "submittedAt"];</code>
     * @return Whether the submittedAt field is set.
     */
    fun hasSubmittedAt(): kotlin.Boolean {
      return _builder.hasSubmittedAt()
    }
  }
}
inline fun gg.dsepractice.chinese.v1alpha1.GameSessionResult.copy(block: gg.dsepractice.chinese.v1alpha1.GameSessionResultKt.Dsl.() -> kotlin.Unit): gg.dsepractice.chinese.v1alpha1.GameSessionResult =
  gg.dsepractice.chinese.v1alpha1.GameSessionResultKt.Dsl._create(this.toBuilder()).apply { block() }._build()

val gg.dsepractice.chinese.v1alpha1.GameSessionResultOrBuilder.timeTakenOrNull: com.google.protobuf.Duration?
  get() = if (hasTimeTaken()) getTimeTaken() else null

val gg.dsepractice.chinese.v1alpha1.GameSessionResultOrBuilder.submittedAtOrNull: com.google.protobuf.Timestamp?
  get() = if (hasSubmittedAt()) getSubmittedAt() else null


package gg.dsepractice.chinese.v1alpha1

import gg.dsepractice.chinese.v1alpha1.CoreServiceGrpc.getServiceDescriptor
import io.grpc.CallOptions
import io.grpc.CallOptions.DEFAULT
import io.grpc.Channel
import io.grpc.Metadata
import io.grpc.MethodDescriptor
import io.grpc.ServerServiceDefinition
import io.grpc.ServerServiceDefinition.builder
import io.grpc.ServiceDescriptor
import io.grpc.Status
import io.grpc.Status.UNIMPLEMENTED
import io.grpc.StatusException
import io.grpc.kotlin.AbstractCoroutineServerImpl
import io.grpc.kotlin.AbstractCoroutineStub
import io.grpc.kotlin.ClientCalls
import io.grpc.kotlin.ClientCalls.unaryRpc
import io.grpc.kotlin.ServerCalls
import io.grpc.kotlin.ServerCalls.unaryServerMethodDefinition
import io.grpc.kotlin.StubFor
import kotlin.String
import kotlin.coroutines.CoroutineContext
import kotlin.coroutines.EmptyCoroutineContext
import kotlin.jvm.JvmOverloads
import kotlin.jvm.JvmStatic

/**
 * Holder for Kotlin coroutine-based client and server APIs for
 * dsepractice.chinese.v1alpha1.CoreService.
 */
public object CoreServiceGrpcKt {
  public const val SERVICE_NAME: String = CoreServiceGrpc.SERVICE_NAME

  @JvmStatic
  public val serviceDescriptor: ServiceDescriptor
    get() = CoreServiceGrpc.getServiceDescriptor()

  public val createArticleMethod: MethodDescriptor<CreateArticleRequest, CreateArticleResponse>
    @JvmStatic
    get() = CoreServiceGrpc.getCreateArticleMethod()

  public val listArticlesMethod: MethodDescriptor<ListArticlesRequest, ListArticlesResponse>
    @JvmStatic
    get() = CoreServiceGrpc.getListArticlesMethod()

  public val getArticleMethod: MethodDescriptor<GetArticleRequest, GetArticleResponse>
    @JvmStatic
    get() = CoreServiceGrpc.getGetArticleMethod()

  public val createGameSessionMethod:
      MethodDescriptor<CreateGameSessionRequest, CreateGameSessionResponse>
    @JvmStatic
    get() = CoreServiceGrpc.getCreateGameSessionMethod()

  public val listActiveGameSessionsMethod:
      MethodDescriptor<ListActiveGameSessionsRequest, ListActiveGameSessionsResponse>
    @JvmStatic
    get() = CoreServiceGrpc.getListActiveGameSessionsMethod()

  public val listGameSessionsMethod:
      MethodDescriptor<ListGameSessionsRequest, ListGameSessionsResponse>
    @JvmStatic
    get() = CoreServiceGrpc.getListGameSessionsMethod()

  public val submitGameSessionResultMethod:
      MethodDescriptor<SubmitGameSessionResultRequest, SubmitGameSessionResultResponse>
    @JvmStatic
    get() = CoreServiceGrpc.getSubmitGameSessionResultMethod()

  public val listLeaderboardEntriesMethod:
      MethodDescriptor<ListLeaderboardEntriesRequest, ListLeaderboardEntriesResponse>
    @JvmStatic
    get() = CoreServiceGrpc.getListLeaderboardEntriesMethod()

  public val getStatisticsMethod: MethodDescriptor<GetStatisticsRequest, GetStatisticsResponse>
    @JvmStatic
    get() = CoreServiceGrpc.getGetStatisticsMethod()

  public val getServerMessageMethod:
      MethodDescriptor<GetServerMessageRequest, GetServerMessageResponse>
    @JvmStatic
    get() = CoreServiceGrpc.getGetServerMessageMethod()

  /**
   * A stub for issuing RPCs to a(n) dsepractice.chinese.v1alpha1.CoreService service as suspending
   * coroutines.
   */
  @StubFor(CoreServiceGrpc::class)
  public class CoreServiceCoroutineStub @JvmOverloads constructor(
    channel: Channel,
    callOptions: CallOptions = DEFAULT,
  ) : AbstractCoroutineStub<CoreServiceCoroutineStub>(channel, callOptions) {
    public override fun build(channel: Channel, callOptions: CallOptions): CoreServiceCoroutineStub
        = CoreServiceCoroutineStub(channel, callOptions)

    /**
     * Executes this RPC and returns the response message, suspending until the RPC completes
     * with [`Status.OK`][Status].  If the RPC completes with another status, a corresponding
     * [StatusException] is thrown.  If this coroutine is cancelled, the RPC is also cancelled
     * with the corresponding exception as a cause.
     *
     * @param request The request message to send to the server.
     *
     * @param headers Metadata to attach to the request.  Most users will not need this.
     *
     * @return The single response from the server.
     */
    public suspend fun createArticle(request: CreateArticleRequest, headers: Metadata = Metadata()):
        CreateArticleResponse = unaryRpc(
      channel,
      CoreServiceGrpc.getCreateArticleMethod(),
      request,
      callOptions,
      headers
    )

    /**
     * Executes this RPC and returns the response message, suspending until the RPC completes
     * with [`Status.OK`][Status].  If the RPC completes with another status, a corresponding
     * [StatusException] is thrown.  If this coroutine is cancelled, the RPC is also cancelled
     * with the corresponding exception as a cause.
     *
     * @param request The request message to send to the server.
     *
     * @param headers Metadata to attach to the request.  Most users will not need this.
     *
     * @return The single response from the server.
     */
    public suspend fun listArticles(request: ListArticlesRequest, headers: Metadata = Metadata()):
        ListArticlesResponse = unaryRpc(
      channel,
      CoreServiceGrpc.getListArticlesMethod(),
      request,
      callOptions,
      headers
    )

    /**
     * Executes this RPC and returns the response message, suspending until the RPC completes
     * with [`Status.OK`][Status].  If the RPC completes with another status, a corresponding
     * [StatusException] is thrown.  If this coroutine is cancelled, the RPC is also cancelled
     * with the corresponding exception as a cause.
     *
     * @param request The request message to send to the server.
     *
     * @param headers Metadata to attach to the request.  Most users will not need this.
     *
     * @return The single response from the server.
     */
    public suspend fun getArticle(request: GetArticleRequest, headers: Metadata = Metadata()):
        GetArticleResponse = unaryRpc(
      channel,
      CoreServiceGrpc.getGetArticleMethod(),
      request,
      callOptions,
      headers
    )

    /**
     * Executes this RPC and returns the response message, suspending until the RPC completes
     * with [`Status.OK`][Status].  If the RPC completes with another status, a corresponding
     * [StatusException] is thrown.  If this coroutine is cancelled, the RPC is also cancelled
     * with the corresponding exception as a cause.
     *
     * @param request The request message to send to the server.
     *
     * @param headers Metadata to attach to the request.  Most users will not need this.
     *
     * @return The single response from the server.
     */
    public suspend fun createGameSession(request: CreateGameSessionRequest, headers: Metadata =
        Metadata()): CreateGameSessionResponse = unaryRpc(
      channel,
      CoreServiceGrpc.getCreateGameSessionMethod(),
      request,
      callOptions,
      headers
    )

    /**
     * Executes this RPC and returns the response message, suspending until the RPC completes
     * with [`Status.OK`][Status].  If the RPC completes with another status, a corresponding
     * [StatusException] is thrown.  If this coroutine is cancelled, the RPC is also cancelled
     * with the corresponding exception as a cause.
     *
     * @param request The request message to send to the server.
     *
     * @param headers Metadata to attach to the request.  Most users will not need this.
     *
     * @return The single response from the server.
     */
    public suspend fun listActiveGameSessions(request: ListActiveGameSessionsRequest,
        headers: Metadata = Metadata()): ListActiveGameSessionsResponse = unaryRpc(
      channel,
      CoreServiceGrpc.getListActiveGameSessionsMethod(),
      request,
      callOptions,
      headers
    )

    /**
     * Executes this RPC and returns the response message, suspending until the RPC completes
     * with [`Status.OK`][Status].  If the RPC completes with another status, a corresponding
     * [StatusException] is thrown.  If this coroutine is cancelled, the RPC is also cancelled
     * with the corresponding exception as a cause.
     *
     * @param request The request message to send to the server.
     *
     * @param headers Metadata to attach to the request.  Most users will not need this.
     *
     * @return The single response from the server.
     */
    public suspend fun listGameSessions(request: ListGameSessionsRequest, headers: Metadata =
        Metadata()): ListGameSessionsResponse = unaryRpc(
      channel,
      CoreServiceGrpc.getListGameSessionsMethod(),
      request,
      callOptions,
      headers
    )

    /**
     * Executes this RPC and returns the response message, suspending until the RPC completes
     * with [`Status.OK`][Status].  If the RPC completes with another status, a corresponding
     * [StatusException] is thrown.  If this coroutine is cancelled, the RPC is also cancelled
     * with the corresponding exception as a cause.
     *
     * @param request The request message to send to the server.
     *
     * @param headers Metadata to attach to the request.  Most users will not need this.
     *
     * @return The single response from the server.
     */
    public suspend fun submitGameSessionResult(request: SubmitGameSessionResultRequest,
        headers: Metadata = Metadata()): SubmitGameSessionResultResponse = unaryRpc(
      channel,
      CoreServiceGrpc.getSubmitGameSessionResultMethod(),
      request,
      callOptions,
      headers
    )

    /**
     * Executes this RPC and returns the response message, suspending until the RPC completes
     * with [`Status.OK`][Status].  If the RPC completes with another status, a corresponding
     * [StatusException] is thrown.  If this coroutine is cancelled, the RPC is also cancelled
     * with the corresponding exception as a cause.
     *
     * @param request The request message to send to the server.
     *
     * @param headers Metadata to attach to the request.  Most users will not need this.
     *
     * @return The single response from the server.
     */
    public suspend fun listLeaderboardEntries(request: ListLeaderboardEntriesRequest,
        headers: Metadata = Metadata()): ListLeaderboardEntriesResponse = unaryRpc(
      channel,
      CoreServiceGrpc.getListLeaderboardEntriesMethod(),
      request,
      callOptions,
      headers
    )

    /**
     * Executes this RPC and returns the response message, suspending until the RPC completes
     * with [`Status.OK`][Status].  If the RPC completes with another status, a corresponding
     * [StatusException] is thrown.  If this coroutine is cancelled, the RPC is also cancelled
     * with the corresponding exception as a cause.
     *
     * @param request The request message to send to the server.
     *
     * @param headers Metadata to attach to the request.  Most users will not need this.
     *
     * @return The single response from the server.
     */
    public suspend fun getStatistics(request: GetStatisticsRequest, headers: Metadata = Metadata()):
        GetStatisticsResponse = unaryRpc(
      channel,
      CoreServiceGrpc.getGetStatisticsMethod(),
      request,
      callOptions,
      headers
    )

    /**
     * Executes this RPC and returns the response message, suspending until the RPC completes
     * with [`Status.OK`][Status].  If the RPC completes with another status, a corresponding
     * [StatusException] is thrown.  If this coroutine is cancelled, the RPC is also cancelled
     * with the corresponding exception as a cause.
     *
     * @param request The request message to send to the server.
     *
     * @param headers Metadata to attach to the request.  Most users will not need this.
     *
     * @return The single response from the server.
     */
    public suspend fun getServerMessage(request: GetServerMessageRequest, headers: Metadata =
        Metadata()): GetServerMessageResponse = unaryRpc(
      channel,
      CoreServiceGrpc.getGetServerMessageMethod(),
      request,
      callOptions,
      headers
    )
  }

  /**
   * Skeletal implementation of the dsepractice.chinese.v1alpha1.CoreService service based on Kotlin
   * coroutines.
   */
  public abstract class CoreServiceCoroutineImplBase(
    coroutineContext: CoroutineContext = EmptyCoroutineContext,
  ) : AbstractCoroutineServerImpl(coroutineContext) {
    /**
     * Returns the response to an RPC for dsepractice.chinese.v1alpha1.CoreService.CreateArticle.
     *
     * If this method fails with a [StatusException], the RPC will fail with the corresponding
     * [Status].  If this method fails with a [java.util.concurrent.CancellationException], the RPC
     * will fail
     * with status `Status.CANCELLED`.  If this method fails for any other reason, the RPC will
     * fail with `Status.UNKNOWN` with the exception as a cause.
     *
     * @param request The request from the client.
     */
    public open suspend fun createArticle(request: CreateArticleRequest): CreateArticleResponse =
        throw
        StatusException(UNIMPLEMENTED.withDescription("Method dsepractice.chinese.v1alpha1.CoreService.CreateArticle is unimplemented"))

    /**
     * Returns the response to an RPC for dsepractice.chinese.v1alpha1.CoreService.ListArticles.
     *
     * If this method fails with a [StatusException], the RPC will fail with the corresponding
     * [Status].  If this method fails with a [java.util.concurrent.CancellationException], the RPC
     * will fail
     * with status `Status.CANCELLED`.  If this method fails for any other reason, the RPC will
     * fail with `Status.UNKNOWN` with the exception as a cause.
     *
     * @param request The request from the client.
     */
    public open suspend fun listArticles(request: ListArticlesRequest): ListArticlesResponse = throw
        StatusException(UNIMPLEMENTED.withDescription("Method dsepractice.chinese.v1alpha1.CoreService.ListArticles is unimplemented"))

    /**
     * Returns the response to an RPC for dsepractice.chinese.v1alpha1.CoreService.GetArticle.
     *
     * If this method fails with a [StatusException], the RPC will fail with the corresponding
     * [Status].  If this method fails with a [java.util.concurrent.CancellationException], the RPC
     * will fail
     * with status `Status.CANCELLED`.  If this method fails for any other reason, the RPC will
     * fail with `Status.UNKNOWN` with the exception as a cause.
     *
     * @param request The request from the client.
     */
    public open suspend fun getArticle(request: GetArticleRequest): GetArticleResponse = throw
        StatusException(UNIMPLEMENTED.withDescription("Method dsepractice.chinese.v1alpha1.CoreService.GetArticle is unimplemented"))

    /**
     * Returns the response to an RPC for
     * dsepractice.chinese.v1alpha1.CoreService.CreateGameSession.
     *
     * If this method fails with a [StatusException], the RPC will fail with the corresponding
     * [Status].  If this method fails with a [java.util.concurrent.CancellationException], the RPC
     * will fail
     * with status `Status.CANCELLED`.  If this method fails for any other reason, the RPC will
     * fail with `Status.UNKNOWN` with the exception as a cause.
     *
     * @param request The request from the client.
     */
    public open suspend fun createGameSession(request: CreateGameSessionRequest):
        CreateGameSessionResponse = throw
        StatusException(UNIMPLEMENTED.withDescription("Method dsepractice.chinese.v1alpha1.CoreService.CreateGameSession is unimplemented"))

    /**
     * Returns the response to an RPC for
     * dsepractice.chinese.v1alpha1.CoreService.ListActiveGameSessions.
     *
     * If this method fails with a [StatusException], the RPC will fail with the corresponding
     * [Status].  If this method fails with a [java.util.concurrent.CancellationException], the RPC
     * will fail
     * with status `Status.CANCELLED`.  If this method fails for any other reason, the RPC will
     * fail with `Status.UNKNOWN` with the exception as a cause.
     *
     * @param request The request from the client.
     */
    public open suspend fun listActiveGameSessions(request: ListActiveGameSessionsRequest):
        ListActiveGameSessionsResponse = throw
        StatusException(UNIMPLEMENTED.withDescription("Method dsepractice.chinese.v1alpha1.CoreService.ListActiveGameSessions is unimplemented"))

    /**
     * Returns the response to an RPC for dsepractice.chinese.v1alpha1.CoreService.ListGameSessions.
     *
     * If this method fails with a [StatusException], the RPC will fail with the corresponding
     * [Status].  If this method fails with a [java.util.concurrent.CancellationException], the RPC
     * will fail
     * with status `Status.CANCELLED`.  If this method fails for any other reason, the RPC will
     * fail with `Status.UNKNOWN` with the exception as a cause.
     *
     * @param request The request from the client.
     */
    public open suspend fun listGameSessions(request: ListGameSessionsRequest):
        ListGameSessionsResponse = throw
        StatusException(UNIMPLEMENTED.withDescription("Method dsepractice.chinese.v1alpha1.CoreService.ListGameSessions is unimplemented"))

    /**
     * Returns the response to an RPC for
     * dsepractice.chinese.v1alpha1.CoreService.SubmitGameSessionResult.
     *
     * If this method fails with a [StatusException], the RPC will fail with the corresponding
     * [Status].  If this method fails with a [java.util.concurrent.CancellationException], the RPC
     * will fail
     * with status `Status.CANCELLED`.  If this method fails for any other reason, the RPC will
     * fail with `Status.UNKNOWN` with the exception as a cause.
     *
     * @param request The request from the client.
     */
    public open suspend fun submitGameSessionResult(request: SubmitGameSessionResultRequest):
        SubmitGameSessionResultResponse = throw
        StatusException(UNIMPLEMENTED.withDescription("Method dsepractice.chinese.v1alpha1.CoreService.SubmitGameSessionResult is unimplemented"))

    /**
     * Returns the response to an RPC for
     * dsepractice.chinese.v1alpha1.CoreService.ListLeaderboardEntries.
     *
     * If this method fails with a [StatusException], the RPC will fail with the corresponding
     * [Status].  If this method fails with a [java.util.concurrent.CancellationException], the RPC
     * will fail
     * with status `Status.CANCELLED`.  If this method fails for any other reason, the RPC will
     * fail with `Status.UNKNOWN` with the exception as a cause.
     *
     * @param request The request from the client.
     */
    public open suspend fun listLeaderboardEntries(request: ListLeaderboardEntriesRequest):
        ListLeaderboardEntriesResponse = throw
        StatusException(UNIMPLEMENTED.withDescription("Method dsepractice.chinese.v1alpha1.CoreService.ListLeaderboardEntries is unimplemented"))

    /**
     * Returns the response to an RPC for dsepractice.chinese.v1alpha1.CoreService.GetStatistics.
     *
     * If this method fails with a [StatusException], the RPC will fail with the corresponding
     * [Status].  If this method fails with a [java.util.concurrent.CancellationException], the RPC
     * will fail
     * with status `Status.CANCELLED`.  If this method fails for any other reason, the RPC will
     * fail with `Status.UNKNOWN` with the exception as a cause.
     *
     * @param request The request from the client.
     */
    public open suspend fun getStatistics(request: GetStatisticsRequest): GetStatisticsResponse =
        throw
        StatusException(UNIMPLEMENTED.withDescription("Method dsepractice.chinese.v1alpha1.CoreService.GetStatistics is unimplemented"))

    /**
     * Returns the response to an RPC for dsepractice.chinese.v1alpha1.CoreService.GetServerMessage.
     *
     * If this method fails with a [StatusException], the RPC will fail with the corresponding
     * [Status].  If this method fails with a [java.util.concurrent.CancellationException], the RPC
     * will fail
     * with status `Status.CANCELLED`.  If this method fails for any other reason, the RPC will
     * fail with `Status.UNKNOWN` with the exception as a cause.
     *
     * @param request The request from the client.
     */
    public open suspend fun getServerMessage(request: GetServerMessageRequest):
        GetServerMessageResponse = throw
        StatusException(UNIMPLEMENTED.withDescription("Method dsepractice.chinese.v1alpha1.CoreService.GetServerMessage is unimplemented"))

    public final override fun bindService(): ServerServiceDefinition =
        builder(getServiceDescriptor())
      .addMethod(unaryServerMethodDefinition(
      context = this.context,
      descriptor = CoreServiceGrpc.getCreateArticleMethod(),
      implementation = ::createArticle
    ))
      .addMethod(unaryServerMethodDefinition(
      context = this.context,
      descriptor = CoreServiceGrpc.getListArticlesMethod(),
      implementation = ::listArticles
    ))
      .addMethod(unaryServerMethodDefinition(
      context = this.context,
      descriptor = CoreServiceGrpc.getGetArticleMethod(),
      implementation = ::getArticle
    ))
      .addMethod(unaryServerMethodDefinition(
      context = this.context,
      descriptor = CoreServiceGrpc.getCreateGameSessionMethod(),
      implementation = ::createGameSession
    ))
      .addMethod(unaryServerMethodDefinition(
      context = this.context,
      descriptor = CoreServiceGrpc.getListActiveGameSessionsMethod(),
      implementation = ::listActiveGameSessions
    ))
      .addMethod(unaryServerMethodDefinition(
      context = this.context,
      descriptor = CoreServiceGrpc.getListGameSessionsMethod(),
      implementation = ::listGameSessions
    ))
      .addMethod(unaryServerMethodDefinition(
      context = this.context,
      descriptor = CoreServiceGrpc.getSubmitGameSessionResultMethod(),
      implementation = ::submitGameSessionResult
    ))
      .addMethod(unaryServerMethodDefinition(
      context = this.context,
      descriptor = CoreServiceGrpc.getListLeaderboardEntriesMethod(),
      implementation = ::listLeaderboardEntries
    ))
      .addMethod(unaryServerMethodDefinition(
      context = this.context,
      descriptor = CoreServiceGrpc.getGetStatisticsMethod(),
      implementation = ::getStatistics
    ))
      .addMethod(unaryServerMethodDefinition(
      context = this.context,
      descriptor = CoreServiceGrpc.getGetServerMessageMethod(),
      implementation = ::getServerMessage
    )).build()
  }
}

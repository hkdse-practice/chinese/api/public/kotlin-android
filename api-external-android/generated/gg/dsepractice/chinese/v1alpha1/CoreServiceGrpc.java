package gg.dsepractice.chinese.v1alpha1;

import static io.grpc.MethodDescriptor.generateFullMethodName;

/**
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.50.2)",
    comments = "Source: dsepractice/chinese/v1alpha1/core_service.proto")
@io.grpc.stub.annotations.GrpcGenerated
public final class CoreServiceGrpc {

  private CoreServiceGrpc() {}

  public static final String SERVICE_NAME = "dsepractice.chinese.v1alpha1.CoreService";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<gg.dsepractice.chinese.v1alpha1.CreateArticleRequest,
      gg.dsepractice.chinese.v1alpha1.CreateArticleResponse> getCreateArticleMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "CreateArticle",
      requestType = gg.dsepractice.chinese.v1alpha1.CreateArticleRequest.class,
      responseType = gg.dsepractice.chinese.v1alpha1.CreateArticleResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<gg.dsepractice.chinese.v1alpha1.CreateArticleRequest,
      gg.dsepractice.chinese.v1alpha1.CreateArticleResponse> getCreateArticleMethod() {
    io.grpc.MethodDescriptor<gg.dsepractice.chinese.v1alpha1.CreateArticleRequest, gg.dsepractice.chinese.v1alpha1.CreateArticleResponse> getCreateArticleMethod;
    if ((getCreateArticleMethod = CoreServiceGrpc.getCreateArticleMethod) == null) {
      synchronized (CoreServiceGrpc.class) {
        if ((getCreateArticleMethod = CoreServiceGrpc.getCreateArticleMethod) == null) {
          CoreServiceGrpc.getCreateArticleMethod = getCreateArticleMethod =
              io.grpc.MethodDescriptor.<gg.dsepractice.chinese.v1alpha1.CreateArticleRequest, gg.dsepractice.chinese.v1alpha1.CreateArticleResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "CreateArticle"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.lite.ProtoLiteUtils.marshaller(
                  gg.dsepractice.chinese.v1alpha1.CreateArticleRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.lite.ProtoLiteUtils.marshaller(
                  gg.dsepractice.chinese.v1alpha1.CreateArticleResponse.getDefaultInstance()))
              .build();
        }
      }
    }
    return getCreateArticleMethod;
  }

  private static volatile io.grpc.MethodDescriptor<gg.dsepractice.chinese.v1alpha1.ListArticlesRequest,
      gg.dsepractice.chinese.v1alpha1.ListArticlesResponse> getListArticlesMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "ListArticles",
      requestType = gg.dsepractice.chinese.v1alpha1.ListArticlesRequest.class,
      responseType = gg.dsepractice.chinese.v1alpha1.ListArticlesResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<gg.dsepractice.chinese.v1alpha1.ListArticlesRequest,
      gg.dsepractice.chinese.v1alpha1.ListArticlesResponse> getListArticlesMethod() {
    io.grpc.MethodDescriptor<gg.dsepractice.chinese.v1alpha1.ListArticlesRequest, gg.dsepractice.chinese.v1alpha1.ListArticlesResponse> getListArticlesMethod;
    if ((getListArticlesMethod = CoreServiceGrpc.getListArticlesMethod) == null) {
      synchronized (CoreServiceGrpc.class) {
        if ((getListArticlesMethod = CoreServiceGrpc.getListArticlesMethod) == null) {
          CoreServiceGrpc.getListArticlesMethod = getListArticlesMethod =
              io.grpc.MethodDescriptor.<gg.dsepractice.chinese.v1alpha1.ListArticlesRequest, gg.dsepractice.chinese.v1alpha1.ListArticlesResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "ListArticles"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.lite.ProtoLiteUtils.marshaller(
                  gg.dsepractice.chinese.v1alpha1.ListArticlesRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.lite.ProtoLiteUtils.marshaller(
                  gg.dsepractice.chinese.v1alpha1.ListArticlesResponse.getDefaultInstance()))
              .build();
        }
      }
    }
    return getListArticlesMethod;
  }

  private static volatile io.grpc.MethodDescriptor<gg.dsepractice.chinese.v1alpha1.GetArticleRequest,
      gg.dsepractice.chinese.v1alpha1.GetArticleResponse> getGetArticleMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "GetArticle",
      requestType = gg.dsepractice.chinese.v1alpha1.GetArticleRequest.class,
      responseType = gg.dsepractice.chinese.v1alpha1.GetArticleResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<gg.dsepractice.chinese.v1alpha1.GetArticleRequest,
      gg.dsepractice.chinese.v1alpha1.GetArticleResponse> getGetArticleMethod() {
    io.grpc.MethodDescriptor<gg.dsepractice.chinese.v1alpha1.GetArticleRequest, gg.dsepractice.chinese.v1alpha1.GetArticleResponse> getGetArticleMethod;
    if ((getGetArticleMethod = CoreServiceGrpc.getGetArticleMethod) == null) {
      synchronized (CoreServiceGrpc.class) {
        if ((getGetArticleMethod = CoreServiceGrpc.getGetArticleMethod) == null) {
          CoreServiceGrpc.getGetArticleMethod = getGetArticleMethod =
              io.grpc.MethodDescriptor.<gg.dsepractice.chinese.v1alpha1.GetArticleRequest, gg.dsepractice.chinese.v1alpha1.GetArticleResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "GetArticle"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.lite.ProtoLiteUtils.marshaller(
                  gg.dsepractice.chinese.v1alpha1.GetArticleRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.lite.ProtoLiteUtils.marshaller(
                  gg.dsepractice.chinese.v1alpha1.GetArticleResponse.getDefaultInstance()))
              .build();
        }
      }
    }
    return getGetArticleMethod;
  }

  private static volatile io.grpc.MethodDescriptor<gg.dsepractice.chinese.v1alpha1.CreateGameSessionRequest,
      gg.dsepractice.chinese.v1alpha1.CreateGameSessionResponse> getCreateGameSessionMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "CreateGameSession",
      requestType = gg.dsepractice.chinese.v1alpha1.CreateGameSessionRequest.class,
      responseType = gg.dsepractice.chinese.v1alpha1.CreateGameSessionResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<gg.dsepractice.chinese.v1alpha1.CreateGameSessionRequest,
      gg.dsepractice.chinese.v1alpha1.CreateGameSessionResponse> getCreateGameSessionMethod() {
    io.grpc.MethodDescriptor<gg.dsepractice.chinese.v1alpha1.CreateGameSessionRequest, gg.dsepractice.chinese.v1alpha1.CreateGameSessionResponse> getCreateGameSessionMethod;
    if ((getCreateGameSessionMethod = CoreServiceGrpc.getCreateGameSessionMethod) == null) {
      synchronized (CoreServiceGrpc.class) {
        if ((getCreateGameSessionMethod = CoreServiceGrpc.getCreateGameSessionMethod) == null) {
          CoreServiceGrpc.getCreateGameSessionMethod = getCreateGameSessionMethod =
              io.grpc.MethodDescriptor.<gg.dsepractice.chinese.v1alpha1.CreateGameSessionRequest, gg.dsepractice.chinese.v1alpha1.CreateGameSessionResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "CreateGameSession"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.lite.ProtoLiteUtils.marshaller(
                  gg.dsepractice.chinese.v1alpha1.CreateGameSessionRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.lite.ProtoLiteUtils.marshaller(
                  gg.dsepractice.chinese.v1alpha1.CreateGameSessionResponse.getDefaultInstance()))
              .build();
        }
      }
    }
    return getCreateGameSessionMethod;
  }

  private static volatile io.grpc.MethodDescriptor<gg.dsepractice.chinese.v1alpha1.ListActiveGameSessionsRequest,
      gg.dsepractice.chinese.v1alpha1.ListActiveGameSessionsResponse> getListActiveGameSessionsMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "ListActiveGameSessions",
      requestType = gg.dsepractice.chinese.v1alpha1.ListActiveGameSessionsRequest.class,
      responseType = gg.dsepractice.chinese.v1alpha1.ListActiveGameSessionsResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<gg.dsepractice.chinese.v1alpha1.ListActiveGameSessionsRequest,
      gg.dsepractice.chinese.v1alpha1.ListActiveGameSessionsResponse> getListActiveGameSessionsMethod() {
    io.grpc.MethodDescriptor<gg.dsepractice.chinese.v1alpha1.ListActiveGameSessionsRequest, gg.dsepractice.chinese.v1alpha1.ListActiveGameSessionsResponse> getListActiveGameSessionsMethod;
    if ((getListActiveGameSessionsMethod = CoreServiceGrpc.getListActiveGameSessionsMethod) == null) {
      synchronized (CoreServiceGrpc.class) {
        if ((getListActiveGameSessionsMethod = CoreServiceGrpc.getListActiveGameSessionsMethod) == null) {
          CoreServiceGrpc.getListActiveGameSessionsMethod = getListActiveGameSessionsMethod =
              io.grpc.MethodDescriptor.<gg.dsepractice.chinese.v1alpha1.ListActiveGameSessionsRequest, gg.dsepractice.chinese.v1alpha1.ListActiveGameSessionsResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "ListActiveGameSessions"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.lite.ProtoLiteUtils.marshaller(
                  gg.dsepractice.chinese.v1alpha1.ListActiveGameSessionsRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.lite.ProtoLiteUtils.marshaller(
                  gg.dsepractice.chinese.v1alpha1.ListActiveGameSessionsResponse.getDefaultInstance()))
              .build();
        }
      }
    }
    return getListActiveGameSessionsMethod;
  }

  private static volatile io.grpc.MethodDescriptor<gg.dsepractice.chinese.v1alpha1.ListGameSessionsRequest,
      gg.dsepractice.chinese.v1alpha1.ListGameSessionsResponse> getListGameSessionsMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "ListGameSessions",
      requestType = gg.dsepractice.chinese.v1alpha1.ListGameSessionsRequest.class,
      responseType = gg.dsepractice.chinese.v1alpha1.ListGameSessionsResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<gg.dsepractice.chinese.v1alpha1.ListGameSessionsRequest,
      gg.dsepractice.chinese.v1alpha1.ListGameSessionsResponse> getListGameSessionsMethod() {
    io.grpc.MethodDescriptor<gg.dsepractice.chinese.v1alpha1.ListGameSessionsRequest, gg.dsepractice.chinese.v1alpha1.ListGameSessionsResponse> getListGameSessionsMethod;
    if ((getListGameSessionsMethod = CoreServiceGrpc.getListGameSessionsMethod) == null) {
      synchronized (CoreServiceGrpc.class) {
        if ((getListGameSessionsMethod = CoreServiceGrpc.getListGameSessionsMethod) == null) {
          CoreServiceGrpc.getListGameSessionsMethod = getListGameSessionsMethod =
              io.grpc.MethodDescriptor.<gg.dsepractice.chinese.v1alpha1.ListGameSessionsRequest, gg.dsepractice.chinese.v1alpha1.ListGameSessionsResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "ListGameSessions"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.lite.ProtoLiteUtils.marshaller(
                  gg.dsepractice.chinese.v1alpha1.ListGameSessionsRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.lite.ProtoLiteUtils.marshaller(
                  gg.dsepractice.chinese.v1alpha1.ListGameSessionsResponse.getDefaultInstance()))
              .build();
        }
      }
    }
    return getListGameSessionsMethod;
  }

  private static volatile io.grpc.MethodDescriptor<gg.dsepractice.chinese.v1alpha1.SubmitGameSessionResultRequest,
      gg.dsepractice.chinese.v1alpha1.SubmitGameSessionResultResponse> getSubmitGameSessionResultMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "SubmitGameSessionResult",
      requestType = gg.dsepractice.chinese.v1alpha1.SubmitGameSessionResultRequest.class,
      responseType = gg.dsepractice.chinese.v1alpha1.SubmitGameSessionResultResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<gg.dsepractice.chinese.v1alpha1.SubmitGameSessionResultRequest,
      gg.dsepractice.chinese.v1alpha1.SubmitGameSessionResultResponse> getSubmitGameSessionResultMethod() {
    io.grpc.MethodDescriptor<gg.dsepractice.chinese.v1alpha1.SubmitGameSessionResultRequest, gg.dsepractice.chinese.v1alpha1.SubmitGameSessionResultResponse> getSubmitGameSessionResultMethod;
    if ((getSubmitGameSessionResultMethod = CoreServiceGrpc.getSubmitGameSessionResultMethod) == null) {
      synchronized (CoreServiceGrpc.class) {
        if ((getSubmitGameSessionResultMethod = CoreServiceGrpc.getSubmitGameSessionResultMethod) == null) {
          CoreServiceGrpc.getSubmitGameSessionResultMethod = getSubmitGameSessionResultMethod =
              io.grpc.MethodDescriptor.<gg.dsepractice.chinese.v1alpha1.SubmitGameSessionResultRequest, gg.dsepractice.chinese.v1alpha1.SubmitGameSessionResultResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "SubmitGameSessionResult"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.lite.ProtoLiteUtils.marshaller(
                  gg.dsepractice.chinese.v1alpha1.SubmitGameSessionResultRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.lite.ProtoLiteUtils.marshaller(
                  gg.dsepractice.chinese.v1alpha1.SubmitGameSessionResultResponse.getDefaultInstance()))
              .build();
        }
      }
    }
    return getSubmitGameSessionResultMethod;
  }

  private static volatile io.grpc.MethodDescriptor<gg.dsepractice.chinese.v1alpha1.ListLeaderboardEntriesRequest,
      gg.dsepractice.chinese.v1alpha1.ListLeaderboardEntriesResponse> getListLeaderboardEntriesMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "ListLeaderboardEntries",
      requestType = gg.dsepractice.chinese.v1alpha1.ListLeaderboardEntriesRequest.class,
      responseType = gg.dsepractice.chinese.v1alpha1.ListLeaderboardEntriesResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<gg.dsepractice.chinese.v1alpha1.ListLeaderboardEntriesRequest,
      gg.dsepractice.chinese.v1alpha1.ListLeaderboardEntriesResponse> getListLeaderboardEntriesMethod() {
    io.grpc.MethodDescriptor<gg.dsepractice.chinese.v1alpha1.ListLeaderboardEntriesRequest, gg.dsepractice.chinese.v1alpha1.ListLeaderboardEntriesResponse> getListLeaderboardEntriesMethod;
    if ((getListLeaderboardEntriesMethod = CoreServiceGrpc.getListLeaderboardEntriesMethod) == null) {
      synchronized (CoreServiceGrpc.class) {
        if ((getListLeaderboardEntriesMethod = CoreServiceGrpc.getListLeaderboardEntriesMethod) == null) {
          CoreServiceGrpc.getListLeaderboardEntriesMethod = getListLeaderboardEntriesMethod =
              io.grpc.MethodDescriptor.<gg.dsepractice.chinese.v1alpha1.ListLeaderboardEntriesRequest, gg.dsepractice.chinese.v1alpha1.ListLeaderboardEntriesResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "ListLeaderboardEntries"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.lite.ProtoLiteUtils.marshaller(
                  gg.dsepractice.chinese.v1alpha1.ListLeaderboardEntriesRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.lite.ProtoLiteUtils.marshaller(
                  gg.dsepractice.chinese.v1alpha1.ListLeaderboardEntriesResponse.getDefaultInstance()))
              .build();
        }
      }
    }
    return getListLeaderboardEntriesMethod;
  }

  private static volatile io.grpc.MethodDescriptor<gg.dsepractice.chinese.v1alpha1.GetStatisticsRequest,
      gg.dsepractice.chinese.v1alpha1.GetStatisticsResponse> getGetStatisticsMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "GetStatistics",
      requestType = gg.dsepractice.chinese.v1alpha1.GetStatisticsRequest.class,
      responseType = gg.dsepractice.chinese.v1alpha1.GetStatisticsResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<gg.dsepractice.chinese.v1alpha1.GetStatisticsRequest,
      gg.dsepractice.chinese.v1alpha1.GetStatisticsResponse> getGetStatisticsMethod() {
    io.grpc.MethodDescriptor<gg.dsepractice.chinese.v1alpha1.GetStatisticsRequest, gg.dsepractice.chinese.v1alpha1.GetStatisticsResponse> getGetStatisticsMethod;
    if ((getGetStatisticsMethod = CoreServiceGrpc.getGetStatisticsMethod) == null) {
      synchronized (CoreServiceGrpc.class) {
        if ((getGetStatisticsMethod = CoreServiceGrpc.getGetStatisticsMethod) == null) {
          CoreServiceGrpc.getGetStatisticsMethod = getGetStatisticsMethod =
              io.grpc.MethodDescriptor.<gg.dsepractice.chinese.v1alpha1.GetStatisticsRequest, gg.dsepractice.chinese.v1alpha1.GetStatisticsResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "GetStatistics"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.lite.ProtoLiteUtils.marshaller(
                  gg.dsepractice.chinese.v1alpha1.GetStatisticsRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.lite.ProtoLiteUtils.marshaller(
                  gg.dsepractice.chinese.v1alpha1.GetStatisticsResponse.getDefaultInstance()))
              .build();
        }
      }
    }
    return getGetStatisticsMethod;
  }

  private static volatile io.grpc.MethodDescriptor<gg.dsepractice.chinese.v1alpha1.GetServerMessageRequest,
      gg.dsepractice.chinese.v1alpha1.GetServerMessageResponse> getGetServerMessageMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "GetServerMessage",
      requestType = gg.dsepractice.chinese.v1alpha1.GetServerMessageRequest.class,
      responseType = gg.dsepractice.chinese.v1alpha1.GetServerMessageResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<gg.dsepractice.chinese.v1alpha1.GetServerMessageRequest,
      gg.dsepractice.chinese.v1alpha1.GetServerMessageResponse> getGetServerMessageMethod() {
    io.grpc.MethodDescriptor<gg.dsepractice.chinese.v1alpha1.GetServerMessageRequest, gg.dsepractice.chinese.v1alpha1.GetServerMessageResponse> getGetServerMessageMethod;
    if ((getGetServerMessageMethod = CoreServiceGrpc.getGetServerMessageMethod) == null) {
      synchronized (CoreServiceGrpc.class) {
        if ((getGetServerMessageMethod = CoreServiceGrpc.getGetServerMessageMethod) == null) {
          CoreServiceGrpc.getGetServerMessageMethod = getGetServerMessageMethod =
              io.grpc.MethodDescriptor.<gg.dsepractice.chinese.v1alpha1.GetServerMessageRequest, gg.dsepractice.chinese.v1alpha1.GetServerMessageResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "GetServerMessage"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.lite.ProtoLiteUtils.marshaller(
                  gg.dsepractice.chinese.v1alpha1.GetServerMessageRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.lite.ProtoLiteUtils.marshaller(
                  gg.dsepractice.chinese.v1alpha1.GetServerMessageResponse.getDefaultInstance()))
              .build();
        }
      }
    }
    return getGetServerMessageMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static CoreServiceStub newStub(io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<CoreServiceStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<CoreServiceStub>() {
        @java.lang.Override
        public CoreServiceStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new CoreServiceStub(channel, callOptions);
        }
      };
    return CoreServiceStub.newStub(factory, channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static CoreServiceBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<CoreServiceBlockingStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<CoreServiceBlockingStub>() {
        @java.lang.Override
        public CoreServiceBlockingStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new CoreServiceBlockingStub(channel, callOptions);
        }
      };
    return CoreServiceBlockingStub.newStub(factory, channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static CoreServiceFutureStub newFutureStub(
      io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<CoreServiceFutureStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<CoreServiceFutureStub>() {
        @java.lang.Override
        public CoreServiceFutureStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new CoreServiceFutureStub(channel, callOptions);
        }
      };
    return CoreServiceFutureStub.newStub(factory, channel);
  }

  /**
   */
  public static abstract class CoreServiceImplBase implements io.grpc.BindableService {

    /**
     */
    public void createArticle(gg.dsepractice.chinese.v1alpha1.CreateArticleRequest request,
        io.grpc.stub.StreamObserver<gg.dsepractice.chinese.v1alpha1.CreateArticleResponse> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getCreateArticleMethod(), responseObserver);
    }

    /**
     */
    public void listArticles(gg.dsepractice.chinese.v1alpha1.ListArticlesRequest request,
        io.grpc.stub.StreamObserver<gg.dsepractice.chinese.v1alpha1.ListArticlesResponse> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getListArticlesMethod(), responseObserver);
    }

    /**
     */
    public void getArticle(gg.dsepractice.chinese.v1alpha1.GetArticleRequest request,
        io.grpc.stub.StreamObserver<gg.dsepractice.chinese.v1alpha1.GetArticleResponse> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getGetArticleMethod(), responseObserver);
    }

    /**
     */
    public void createGameSession(gg.dsepractice.chinese.v1alpha1.CreateGameSessionRequest request,
        io.grpc.stub.StreamObserver<gg.dsepractice.chinese.v1alpha1.CreateGameSessionResponse> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getCreateGameSessionMethod(), responseObserver);
    }

    /**
     */
    public void listActiveGameSessions(gg.dsepractice.chinese.v1alpha1.ListActiveGameSessionsRequest request,
        io.grpc.stub.StreamObserver<gg.dsepractice.chinese.v1alpha1.ListActiveGameSessionsResponse> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getListActiveGameSessionsMethod(), responseObserver);
    }

    /**
     */
    public void listGameSessions(gg.dsepractice.chinese.v1alpha1.ListGameSessionsRequest request,
        io.grpc.stub.StreamObserver<gg.dsepractice.chinese.v1alpha1.ListGameSessionsResponse> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getListGameSessionsMethod(), responseObserver);
    }

    /**
     */
    public void submitGameSessionResult(gg.dsepractice.chinese.v1alpha1.SubmitGameSessionResultRequest request,
        io.grpc.stub.StreamObserver<gg.dsepractice.chinese.v1alpha1.SubmitGameSessionResultResponse> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getSubmitGameSessionResultMethod(), responseObserver);
    }

    /**
     */
    public void listLeaderboardEntries(gg.dsepractice.chinese.v1alpha1.ListLeaderboardEntriesRequest request,
        io.grpc.stub.StreamObserver<gg.dsepractice.chinese.v1alpha1.ListLeaderboardEntriesResponse> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getListLeaderboardEntriesMethod(), responseObserver);
    }

    /**
     */
    public void getStatistics(gg.dsepractice.chinese.v1alpha1.GetStatisticsRequest request,
        io.grpc.stub.StreamObserver<gg.dsepractice.chinese.v1alpha1.GetStatisticsResponse> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getGetStatisticsMethod(), responseObserver);
    }

    /**
     */
    public void getServerMessage(gg.dsepractice.chinese.v1alpha1.GetServerMessageRequest request,
        io.grpc.stub.StreamObserver<gg.dsepractice.chinese.v1alpha1.GetServerMessageResponse> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getGetServerMessageMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getCreateArticleMethod(),
            io.grpc.stub.ServerCalls.asyncUnaryCall(
              new MethodHandlers<
                gg.dsepractice.chinese.v1alpha1.CreateArticleRequest,
                gg.dsepractice.chinese.v1alpha1.CreateArticleResponse>(
                  this, METHODID_CREATE_ARTICLE)))
          .addMethod(
            getListArticlesMethod(),
            io.grpc.stub.ServerCalls.asyncUnaryCall(
              new MethodHandlers<
                gg.dsepractice.chinese.v1alpha1.ListArticlesRequest,
                gg.dsepractice.chinese.v1alpha1.ListArticlesResponse>(
                  this, METHODID_LIST_ARTICLES)))
          .addMethod(
            getGetArticleMethod(),
            io.grpc.stub.ServerCalls.asyncUnaryCall(
              new MethodHandlers<
                gg.dsepractice.chinese.v1alpha1.GetArticleRequest,
                gg.dsepractice.chinese.v1alpha1.GetArticleResponse>(
                  this, METHODID_GET_ARTICLE)))
          .addMethod(
            getCreateGameSessionMethod(),
            io.grpc.stub.ServerCalls.asyncUnaryCall(
              new MethodHandlers<
                gg.dsepractice.chinese.v1alpha1.CreateGameSessionRequest,
                gg.dsepractice.chinese.v1alpha1.CreateGameSessionResponse>(
                  this, METHODID_CREATE_GAME_SESSION)))
          .addMethod(
            getListActiveGameSessionsMethod(),
            io.grpc.stub.ServerCalls.asyncUnaryCall(
              new MethodHandlers<
                gg.dsepractice.chinese.v1alpha1.ListActiveGameSessionsRequest,
                gg.dsepractice.chinese.v1alpha1.ListActiveGameSessionsResponse>(
                  this, METHODID_LIST_ACTIVE_GAME_SESSIONS)))
          .addMethod(
            getListGameSessionsMethod(),
            io.grpc.stub.ServerCalls.asyncUnaryCall(
              new MethodHandlers<
                gg.dsepractice.chinese.v1alpha1.ListGameSessionsRequest,
                gg.dsepractice.chinese.v1alpha1.ListGameSessionsResponse>(
                  this, METHODID_LIST_GAME_SESSIONS)))
          .addMethod(
            getSubmitGameSessionResultMethod(),
            io.grpc.stub.ServerCalls.asyncUnaryCall(
              new MethodHandlers<
                gg.dsepractice.chinese.v1alpha1.SubmitGameSessionResultRequest,
                gg.dsepractice.chinese.v1alpha1.SubmitGameSessionResultResponse>(
                  this, METHODID_SUBMIT_GAME_SESSION_RESULT)))
          .addMethod(
            getListLeaderboardEntriesMethod(),
            io.grpc.stub.ServerCalls.asyncUnaryCall(
              new MethodHandlers<
                gg.dsepractice.chinese.v1alpha1.ListLeaderboardEntriesRequest,
                gg.dsepractice.chinese.v1alpha1.ListLeaderboardEntriesResponse>(
                  this, METHODID_LIST_LEADERBOARD_ENTRIES)))
          .addMethod(
            getGetStatisticsMethod(),
            io.grpc.stub.ServerCalls.asyncUnaryCall(
              new MethodHandlers<
                gg.dsepractice.chinese.v1alpha1.GetStatisticsRequest,
                gg.dsepractice.chinese.v1alpha1.GetStatisticsResponse>(
                  this, METHODID_GET_STATISTICS)))
          .addMethod(
            getGetServerMessageMethod(),
            io.grpc.stub.ServerCalls.asyncUnaryCall(
              new MethodHandlers<
                gg.dsepractice.chinese.v1alpha1.GetServerMessageRequest,
                gg.dsepractice.chinese.v1alpha1.GetServerMessageResponse>(
                  this, METHODID_GET_SERVER_MESSAGE)))
          .build();
    }
  }

  /**
   */
  public static final class CoreServiceStub extends io.grpc.stub.AbstractAsyncStub<CoreServiceStub> {
    private CoreServiceStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected CoreServiceStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new CoreServiceStub(channel, callOptions);
    }

    /**
     */
    public void createArticle(gg.dsepractice.chinese.v1alpha1.CreateArticleRequest request,
        io.grpc.stub.StreamObserver<gg.dsepractice.chinese.v1alpha1.CreateArticleResponse> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getCreateArticleMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void listArticles(gg.dsepractice.chinese.v1alpha1.ListArticlesRequest request,
        io.grpc.stub.StreamObserver<gg.dsepractice.chinese.v1alpha1.ListArticlesResponse> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getListArticlesMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void getArticle(gg.dsepractice.chinese.v1alpha1.GetArticleRequest request,
        io.grpc.stub.StreamObserver<gg.dsepractice.chinese.v1alpha1.GetArticleResponse> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getGetArticleMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void createGameSession(gg.dsepractice.chinese.v1alpha1.CreateGameSessionRequest request,
        io.grpc.stub.StreamObserver<gg.dsepractice.chinese.v1alpha1.CreateGameSessionResponse> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getCreateGameSessionMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void listActiveGameSessions(gg.dsepractice.chinese.v1alpha1.ListActiveGameSessionsRequest request,
        io.grpc.stub.StreamObserver<gg.dsepractice.chinese.v1alpha1.ListActiveGameSessionsResponse> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getListActiveGameSessionsMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void listGameSessions(gg.dsepractice.chinese.v1alpha1.ListGameSessionsRequest request,
        io.grpc.stub.StreamObserver<gg.dsepractice.chinese.v1alpha1.ListGameSessionsResponse> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getListGameSessionsMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void submitGameSessionResult(gg.dsepractice.chinese.v1alpha1.SubmitGameSessionResultRequest request,
        io.grpc.stub.StreamObserver<gg.dsepractice.chinese.v1alpha1.SubmitGameSessionResultResponse> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getSubmitGameSessionResultMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void listLeaderboardEntries(gg.dsepractice.chinese.v1alpha1.ListLeaderboardEntriesRequest request,
        io.grpc.stub.StreamObserver<gg.dsepractice.chinese.v1alpha1.ListLeaderboardEntriesResponse> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getListLeaderboardEntriesMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void getStatistics(gg.dsepractice.chinese.v1alpha1.GetStatisticsRequest request,
        io.grpc.stub.StreamObserver<gg.dsepractice.chinese.v1alpha1.GetStatisticsResponse> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getGetStatisticsMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void getServerMessage(gg.dsepractice.chinese.v1alpha1.GetServerMessageRequest request,
        io.grpc.stub.StreamObserver<gg.dsepractice.chinese.v1alpha1.GetServerMessageResponse> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getGetServerMessageMethod(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   */
  public static final class CoreServiceBlockingStub extends io.grpc.stub.AbstractBlockingStub<CoreServiceBlockingStub> {
    private CoreServiceBlockingStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected CoreServiceBlockingStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new CoreServiceBlockingStub(channel, callOptions);
    }

    /**
     */
    public gg.dsepractice.chinese.v1alpha1.CreateArticleResponse createArticle(gg.dsepractice.chinese.v1alpha1.CreateArticleRequest request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getCreateArticleMethod(), getCallOptions(), request);
    }

    /**
     */
    public gg.dsepractice.chinese.v1alpha1.ListArticlesResponse listArticles(gg.dsepractice.chinese.v1alpha1.ListArticlesRequest request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getListArticlesMethod(), getCallOptions(), request);
    }

    /**
     */
    public gg.dsepractice.chinese.v1alpha1.GetArticleResponse getArticle(gg.dsepractice.chinese.v1alpha1.GetArticleRequest request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getGetArticleMethod(), getCallOptions(), request);
    }

    /**
     */
    public gg.dsepractice.chinese.v1alpha1.CreateGameSessionResponse createGameSession(gg.dsepractice.chinese.v1alpha1.CreateGameSessionRequest request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getCreateGameSessionMethod(), getCallOptions(), request);
    }

    /**
     */
    public gg.dsepractice.chinese.v1alpha1.ListActiveGameSessionsResponse listActiveGameSessions(gg.dsepractice.chinese.v1alpha1.ListActiveGameSessionsRequest request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getListActiveGameSessionsMethod(), getCallOptions(), request);
    }

    /**
     */
    public gg.dsepractice.chinese.v1alpha1.ListGameSessionsResponse listGameSessions(gg.dsepractice.chinese.v1alpha1.ListGameSessionsRequest request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getListGameSessionsMethod(), getCallOptions(), request);
    }

    /**
     */
    public gg.dsepractice.chinese.v1alpha1.SubmitGameSessionResultResponse submitGameSessionResult(gg.dsepractice.chinese.v1alpha1.SubmitGameSessionResultRequest request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getSubmitGameSessionResultMethod(), getCallOptions(), request);
    }

    /**
     */
    public gg.dsepractice.chinese.v1alpha1.ListLeaderboardEntriesResponse listLeaderboardEntries(gg.dsepractice.chinese.v1alpha1.ListLeaderboardEntriesRequest request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getListLeaderboardEntriesMethod(), getCallOptions(), request);
    }

    /**
     */
    public gg.dsepractice.chinese.v1alpha1.GetStatisticsResponse getStatistics(gg.dsepractice.chinese.v1alpha1.GetStatisticsRequest request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getGetStatisticsMethod(), getCallOptions(), request);
    }

    /**
     */
    public gg.dsepractice.chinese.v1alpha1.GetServerMessageResponse getServerMessage(gg.dsepractice.chinese.v1alpha1.GetServerMessageRequest request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getGetServerMessageMethod(), getCallOptions(), request);
    }
  }

  /**
   */
  public static final class CoreServiceFutureStub extends io.grpc.stub.AbstractFutureStub<CoreServiceFutureStub> {
    private CoreServiceFutureStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected CoreServiceFutureStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new CoreServiceFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<gg.dsepractice.chinese.v1alpha1.CreateArticleResponse> createArticle(
        gg.dsepractice.chinese.v1alpha1.CreateArticleRequest request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getCreateArticleMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<gg.dsepractice.chinese.v1alpha1.ListArticlesResponse> listArticles(
        gg.dsepractice.chinese.v1alpha1.ListArticlesRequest request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getListArticlesMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<gg.dsepractice.chinese.v1alpha1.GetArticleResponse> getArticle(
        gg.dsepractice.chinese.v1alpha1.GetArticleRequest request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getGetArticleMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<gg.dsepractice.chinese.v1alpha1.CreateGameSessionResponse> createGameSession(
        gg.dsepractice.chinese.v1alpha1.CreateGameSessionRequest request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getCreateGameSessionMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<gg.dsepractice.chinese.v1alpha1.ListActiveGameSessionsResponse> listActiveGameSessions(
        gg.dsepractice.chinese.v1alpha1.ListActiveGameSessionsRequest request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getListActiveGameSessionsMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<gg.dsepractice.chinese.v1alpha1.ListGameSessionsResponse> listGameSessions(
        gg.dsepractice.chinese.v1alpha1.ListGameSessionsRequest request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getListGameSessionsMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<gg.dsepractice.chinese.v1alpha1.SubmitGameSessionResultResponse> submitGameSessionResult(
        gg.dsepractice.chinese.v1alpha1.SubmitGameSessionResultRequest request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getSubmitGameSessionResultMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<gg.dsepractice.chinese.v1alpha1.ListLeaderboardEntriesResponse> listLeaderboardEntries(
        gg.dsepractice.chinese.v1alpha1.ListLeaderboardEntriesRequest request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getListLeaderboardEntriesMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<gg.dsepractice.chinese.v1alpha1.GetStatisticsResponse> getStatistics(
        gg.dsepractice.chinese.v1alpha1.GetStatisticsRequest request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getGetStatisticsMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<gg.dsepractice.chinese.v1alpha1.GetServerMessageResponse> getServerMessage(
        gg.dsepractice.chinese.v1alpha1.GetServerMessageRequest request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getGetServerMessageMethod(), getCallOptions()), request);
    }
  }

  private static final int METHODID_CREATE_ARTICLE = 0;
  private static final int METHODID_LIST_ARTICLES = 1;
  private static final int METHODID_GET_ARTICLE = 2;
  private static final int METHODID_CREATE_GAME_SESSION = 3;
  private static final int METHODID_LIST_ACTIVE_GAME_SESSIONS = 4;
  private static final int METHODID_LIST_GAME_SESSIONS = 5;
  private static final int METHODID_SUBMIT_GAME_SESSION_RESULT = 6;
  private static final int METHODID_LIST_LEADERBOARD_ENTRIES = 7;
  private static final int METHODID_GET_STATISTICS = 8;
  private static final int METHODID_GET_SERVER_MESSAGE = 9;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final CoreServiceImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(CoreServiceImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_CREATE_ARTICLE:
          serviceImpl.createArticle((gg.dsepractice.chinese.v1alpha1.CreateArticleRequest) request,
              (io.grpc.stub.StreamObserver<gg.dsepractice.chinese.v1alpha1.CreateArticleResponse>) responseObserver);
          break;
        case METHODID_LIST_ARTICLES:
          serviceImpl.listArticles((gg.dsepractice.chinese.v1alpha1.ListArticlesRequest) request,
              (io.grpc.stub.StreamObserver<gg.dsepractice.chinese.v1alpha1.ListArticlesResponse>) responseObserver);
          break;
        case METHODID_GET_ARTICLE:
          serviceImpl.getArticle((gg.dsepractice.chinese.v1alpha1.GetArticleRequest) request,
              (io.grpc.stub.StreamObserver<gg.dsepractice.chinese.v1alpha1.GetArticleResponse>) responseObserver);
          break;
        case METHODID_CREATE_GAME_SESSION:
          serviceImpl.createGameSession((gg.dsepractice.chinese.v1alpha1.CreateGameSessionRequest) request,
              (io.grpc.stub.StreamObserver<gg.dsepractice.chinese.v1alpha1.CreateGameSessionResponse>) responseObserver);
          break;
        case METHODID_LIST_ACTIVE_GAME_SESSIONS:
          serviceImpl.listActiveGameSessions((gg.dsepractice.chinese.v1alpha1.ListActiveGameSessionsRequest) request,
              (io.grpc.stub.StreamObserver<gg.dsepractice.chinese.v1alpha1.ListActiveGameSessionsResponse>) responseObserver);
          break;
        case METHODID_LIST_GAME_SESSIONS:
          serviceImpl.listGameSessions((gg.dsepractice.chinese.v1alpha1.ListGameSessionsRequest) request,
              (io.grpc.stub.StreamObserver<gg.dsepractice.chinese.v1alpha1.ListGameSessionsResponse>) responseObserver);
          break;
        case METHODID_SUBMIT_GAME_SESSION_RESULT:
          serviceImpl.submitGameSessionResult((gg.dsepractice.chinese.v1alpha1.SubmitGameSessionResultRequest) request,
              (io.grpc.stub.StreamObserver<gg.dsepractice.chinese.v1alpha1.SubmitGameSessionResultResponse>) responseObserver);
          break;
        case METHODID_LIST_LEADERBOARD_ENTRIES:
          serviceImpl.listLeaderboardEntries((gg.dsepractice.chinese.v1alpha1.ListLeaderboardEntriesRequest) request,
              (io.grpc.stub.StreamObserver<gg.dsepractice.chinese.v1alpha1.ListLeaderboardEntriesResponse>) responseObserver);
          break;
        case METHODID_GET_STATISTICS:
          serviceImpl.getStatistics((gg.dsepractice.chinese.v1alpha1.GetStatisticsRequest) request,
              (io.grpc.stub.StreamObserver<gg.dsepractice.chinese.v1alpha1.GetStatisticsResponse>) responseObserver);
          break;
        case METHODID_GET_SERVER_MESSAGE:
          serviceImpl.getServerMessage((gg.dsepractice.chinese.v1alpha1.GetServerMessageRequest) request,
              (io.grpc.stub.StreamObserver<gg.dsepractice.chinese.v1alpha1.GetServerMessageResponse>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (CoreServiceGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .addMethod(getCreateArticleMethod())
              .addMethod(getListArticlesMethod())
              .addMethod(getGetArticleMethod())
              .addMethod(getCreateGameSessionMethod())
              .addMethod(getListActiveGameSessionsMethod())
              .addMethod(getListGameSessionsMethod())
              .addMethod(getSubmitGameSessionResultMethod())
              .addMethod(getListLeaderboardEntriesMethod())
              .addMethod(getGetStatisticsMethod())
              .addMethod(getGetServerMessageMethod())
              .build();
        }
      }
    }
    return result;
  }
}

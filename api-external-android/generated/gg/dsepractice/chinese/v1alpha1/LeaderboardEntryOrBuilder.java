// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: dsepractice/chinese/v1alpha1/core_types.proto

package gg.dsepractice.chinese.v1alpha1;

public interface LeaderboardEntryOrBuilder extends
    // @@protoc_insertion_point(interface_extends:dsepractice.chinese.v1alpha1.LeaderboardEntry)
    com.google.protobuf.MessageLiteOrBuilder {

  /**
   * <code>bool is_current_user = 1 [json_name = "isCurrentUser"];</code>
   * @return The isCurrentUser.
   */
  boolean getIsCurrentUser();

  /**
   * <code>int64 score = 2 [json_name = "score"];</code>
   * @return The score.
   */
  long getScore();

  /**
   * <code>.google.protobuf.Duration time_taken = 3 [json_name = "timeTaken"];</code>
   * @return Whether the timeTaken field is set.
   */
  boolean hasTimeTaken();
  /**
   * <code>.google.protobuf.Duration time_taken = 3 [json_name = "timeTaken"];</code>
   * @return The timeTaken.
   */
  com.google.protobuf.Duration getTimeTaken();
}

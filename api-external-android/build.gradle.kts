plugins {
    // Apply the org.jetbrains.kotlin.jvm Plugin to add support for Kotlin.
    id("org.jetbrains.kotlin.jvm") version "1.6.0"

    // Apply the java-library plugin for API and implementation separation.
    id("java-library")

    java
    `maven-publish`
}

val projVersion: String by project

version = if (project.hasProperty("projVersion")) projVersion else "0.0.0"
group = "gg.dsepractice"

val ciApiV4Url = System.getenv("CI_API_V4_URL") ?: "https://gitlab.com/api/v4"
val ciGroupId = "56964411"
val ciProjectId = "40619367"

repositories {
    // Use Maven Central for resolving dependencies.
    mavenCentral()

    maven {
        url = uri("${ciApiV4Url}/groups/${ciGroupId}/-/packages/maven")
        name = "GitLab"
        credentials(HttpHeaderCredentials::class) {
            name = "Job-Token"
            value = System.getenv("CI_JOB_TOKEN")
        }
        authentication {
            create("header", HttpHeaderAuthentication::class)
        }
    }
}

publishing {
    publications {
        create<MavenPublication>("library") {
            from(components["java"])
        }
    }
    repositories {
        maven {
            url = uri("${ciApiV4Url}/projects/${ciProjectId}/packages/maven")
            name = "GitLab"
            credentials(HttpHeaderCredentials::class) {
                name = "Job-Token"
                value = System.getenv("CI_JOB_TOKEN")
            }
            authentication {
                create("header", HttpHeaderAuthentication::class)
            }
        }
    }
}

val grpcVersion = "1.50.2"
val grpcKotlinVersion = "1.3.0"
val protobufVersion = "3.21.9"
val coroutinesVersion = "1.6.4"

dependencies {
    // Align versions of all Kotlin components
    implementation(platform("org.jetbrains.kotlin:kotlin-bom"))

    // This dependency is used internally, and not exposed to consumers on their own compile classpath.
    implementation("com.google.guava:guava:30.1.1-jre")

    // Use the Kotlin test library.
    testImplementation("org.jetbrains.kotlin:kotlin-test")

    // Use the Kotlin JUnit integration.
    testImplementation("org.jetbrains.kotlin:kotlin-test-junit")

    // This dependency is exported to consumers, that is to say found on their compile classpath.
    api("org.apache.commons:commons-math3:3.6.1")

    api("org.jetbrains.kotlinx:kotlinx-coroutines-android:${coroutinesVersion}")
    api("io.grpc:grpc-stub:${grpcVersion}")
    api("io.grpc:grpc-kotlin-stub:${grpcKotlinVersion}")
    api("io.grpc:grpc-protobuf-lite:${grpcVersion}")
    api("com.google.protobuf:protobuf-kotlin-lite:${protobufVersion}")

}

java {
    withSourcesJar()
    sourceCompatibility = JavaVersion.VERSION_11
    targetCompatibility = JavaVersion.VERSION_11
}

tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile>().configureEach {
    kotlinOptions {
        jvmTarget = "11"
        freeCompilerArgs += "-Xopt-in=kotlin.RequiresOptIn"
    }
}

sourceSets {
    main {
        java {
            srcDir("generated")
        }
    }
}


tasks.create<Wrapper>("wrapper") {
    gradleVersion = "7.5.1"
}
